PROGS=chrootpwd
default: $(PROGS)

CFLAGS=-Wall

-include config.mk

VERSION:=$(shell git describe --always --tags --dirty)
DATETIME:=$(shell date '+%a %d %b %Y %H:%M:%S %Z')

CPPFLAGS += -DVERSION='"$(VERSION)"' -DDATETIME='"$(DATETIME)"'

.PHONY: all clean install
all: $(PROGS)

install: $(PROGS)
	install -o root -g root -m u+rs,g+rx,o+rx $(PROGS) /usr/bin

clean:
	rm -f $(PROGS)
