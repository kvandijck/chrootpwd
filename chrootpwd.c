#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <sched.h>
#include <syslog.h>
#include <fcntl.h>
#include <syslog.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <sys/syscall.h>
#include <linux/sched.h>

#define NAME "chrootpwd"
#ifndef VERSION
#define VERSION "?"
#endif
#ifndef DATETIME
#define DATETIME (__DATE__ ", " __TIME__)
#endif

static int max_loglevel = LOG_WARNING;

__attribute((format(printf,2,3)))
void mylog(int loglevel, const char *fmt, ...)
{
	static int logtostderr = -1;
	va_list va;
	char *str;

	if (logtostderr < 0) {
		int fd;

		fd = open("/dev/tty", O_RDONLY | O_NOCTTY);
		if (fd >= 0)
			close(fd);
		/* log to stderr when we're in a terminal */
		logtostderr = fd >= 0;
		if (!logtostderr) {
			openlog(NAME, 0, LOG_LOCAL2);
			setlogmask(LOG_UPTO(max_loglevel));
		}
	}

	/* render string */
	va_start(va, fmt);
	vasprintf(&str, fmt, va);
	va_end(va);

	if (!logtostderr) {
		syslog(loglevel, "%s", str);

	} else if (loglevel <= max_loglevel) {
		struct timespec tv;
		static char timbuf[64];

		clock_gettime(CLOCK_REALTIME, &tv);
		strftime(timbuf, sizeof(timbuf), "%b %d %H:%M:%S", localtime(&tv.tv_sec));
		sprintf(timbuf+strlen(timbuf), ".%03u ", (int)(tv.tv_nsec/1000000));

		struct iovec vec[] = {
			{ .iov_base = timbuf, .iov_len = strlen(timbuf), },
			{ .iov_base = NAME, .iov_len = strlen(NAME), },
			{ .iov_base = ": ", .iov_len = 2, },
			{ .iov_base = str, .iov_len = strlen(str), },
			{ .iov_base = "\n", .iov_len = 1, },
		};
		writev(STDERR_FILENO, vec, sizeof(vec)/sizeof(vec[0]));
	}

	free(str);
	if ((loglevel & LOG_PRIMASK) <= LOG_ERR)
		exit(1);
}
#define ESTR(x) strerror(x)

/* modify scheduler: don't rely on libc implemenation */
#ifndef __NR_unshare
/* XXX use the proper syscall numbers */
#ifdef __x86_64__
#define __NR_unshare		310
#endif
#ifdef __i386__
#define __NR_unshare		310
#endif
#ifdef __arm__
#define __NR_unshare		337
#endif
#endif

int unshare(int flags)
{
	return syscall(__NR_unshare, flags);
}

static int mkdir_p(char *path, int mode)
{
	int ret;
	char *end;

	/* skip initial / */
	for (end = path; *end == '/'; ++end) {}

	for (end = strchr(end, '/'); end; end = strchr(end+1, '/')) {
		*end = 0;
		ret = mkdir(path, mode);
		if (ret < 0 && errno != EEXIST)
			mylog(LOG_ERR, "mkdir %s: %s", path, ESTR(errno));
		*end = '/';
	}
	return 0;
}

/* program options */
static const char help_msg[] =
	NAME ": chroot and bind-mount current directory into new namespace\n"
	"usage:	" NAME " [OPTIONS ...] NEWROOT [ BINARY ...]\n"
	"Options\n"
	" -i DIR	bind-mount DIR under NEWROOT too\n"
	" -s		bind-mount std dirs: /dev, /proc, /sys\n"
	" -v		verbose\n"
	;
static const char optstring[] = "+?Vvi:s";

struct name {
	struct name *next;
	char str[1];
};
static struct name *names;

static void remember_name(struct name **pname, const char *str)
{
	struct name *name;

	name = malloc(sizeof(*name) + strlen(str));
	memset(name, 0, sizeof(*name));
	name->next = *pname;
	*pname = name;
	strcpy(name->str, str);
}

int main(int argc, char *argv[])
{
	int ret;
	int uid;
	int opt;
	char *pwd;
	char *newroot;
	char *dir, *newdir;

	pwd = getcwd(NULL, 0);
	uid = getuid();
	if (strcmp(pwd, "/") != 0)
		/* keep pwd if not / */
		remember_name(&names, pwd);

	/* parse program options */
	while ((opt = getopt(argc, argv, optstring)) != -1)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s: %s, %s\n", NAME, VERSION, DATETIME);
		return 0;
	default:
		fprintf(stderr, "%s: option '%c' unrecognised\n", NAME, opt);
	case '?':
		fputs(help_msg, stderr);
		exit(1);
		break;
	case 'v':
		++max_loglevel;
		break;
	case 'i':
		remember_name(&names, optarg);
		break;
	case 's':
		remember_name(&names, "/dev/shm");
		remember_name(&names, "/dev");
		remember_name(&names, "/proc");
		remember_name(&names, "/sys");
		break;
	}

	mylog(LOG_INFO, "unshare NEWNS");
	ret = unshare(CLONE_NEWNS);
	if (ret)
		mylog(LOG_ERR, "unshare NEWNS: %s", ESTR(errno));

	mylog(LOG_INFO, "make / private");
	ret = mount("none", "/", NULL, MS_REC | MS_PRIVATE, NULL);
	if (ret < 0)
		mylog(LOG_ERR, "mount / MS_PRIVATE: %s", ESTR(errno));

	newroot = argv[optind];
	if (!newroot)
		mylog(LOG_ERR, "no new root given");
	/* do custom mounts between unshare and chroot */
	struct stat st;
	struct name *name;

	for (name = names; name; name = name->next) {
		dir = name->str;
		if (stat(dir, &st) < 0) {
			mylog(LOG_WARNING, "ignoring -i '%s': %s", dir, ESTR(errno));
			continue;
		}
		if (!S_ISDIR(st.st_mode) && !S_ISREG(st.st_mode)) {
			mylog(LOG_WARNING, "ignoring -i '%s', not a file or directory", dir);
			continue;
		}
		asprintf(&newdir, "%s%s%s", newroot, (*dir == '/') ? "" : "/", dir);
		mylog(LOG_INFO, "mkdir -p or touch %s", newdir);
		ret = mkdir_p(newdir, 0777);
		if (ret < 0 && errno != EEXIST)
			mylog(LOG_ERR, "mkdir %s: %s", newdir, ESTR(errno));
		/* create final component */
		if (S_ISDIR(st.st_mode)) {
			ret = mkdir(newdir, 0777);
			if (ret < 0 && errno != EEXIST)
				mylog(LOG_ERR, "mkdir %s: %s", newdir, ESTR(errno));
		} else {
			int fd;
			fd = open(newdir, O_CREAT, 0777);
			if (fd < 0)
				mylog(LOG_ERR, "open %s: %s", newdir, ESTR(errno));
			close(fd);
		}
		mylog(LOG_INFO, "mount %s %s", dir, newdir);
		ret = mount(dir, newdir, NULL, MS_BIND, NULL);
		if (ret < 0)
			mylog(LOG_ERR, "bind-mount %s to %s: %s", dir, newdir, ESTR(errno));
		free(newdir);
	}

	mylog(LOG_INFO, "chroot %s", newroot);
	ret = chroot(newroot);
	if (ret < 0)
		mylog(LOG_ERR, "chroot '%s': %s", newroot, ESTR(errno));

	/* chdir back, and loose original '.' */
	mylog(LOG_INFO, "chdir %s", pwd);
	ret = chdir(pwd);
	if (ret < 0)
		mylog(LOG_ERR, "chdir . to %s failed: %s", pwd, ESTR(errno));

	mylog(LOG_INFO, "setuid %u", uid);
	ret = setuid(uid);
	if (ret < 0)
		mylog(LOG_ERR, "setuid '%u': %s", uid, ESTR(errno));

	argv += optind+1;
	if (!*argv)
		/* assign default command: sh */
		argv = (char *[]){ "sh", NULL, };
	mylog(LOG_INFO, "execvp %s ...", *argv);
	execvp(*argv, argv);
	mylog(LOG_ERR, "execvp %s ...: %s", *argv, ESTR(errno));
	return 1;
}
